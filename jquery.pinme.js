/*
    jQuery pin me plugin
    
    Adds a pin it button to single images to easily share them on Pinterest.
    
    Author  : Germano Guerrini
    Version : 1.0
*/
;(function( $ ){
    'use strict';
    $.fn.pinMe = function( options ) {
        var defaults = {
                imageMediaAttribute: 'src',
                imageDescriptionAttribute: 'data-pin-description',
                buttonUrl: 'img/pinit.png',
                buttonWidth: 49,
                buttonHeight: 25,
                buttonAlignment: 'top-left',
                buttonPadding: 5,
                buttonWillShowFunction: function(button) {
                    button.css({
                        opacity: 0.75
                    });
                },
                buttonOnMouseOverFunction: function(button, image) {
                    button.css({
                        opacity: 1.0,
                        '-webkit-transition': 'opacity .25s ease-out',
                        '-moz-transition': 'opacity .25s ease-out',
                        transition: 'opacity .25s ease-out'
                    });
                    image.css({
                        opacity:0.75,
                        '-webkit-transition': 'opacity .25s ease-out',
                        '-moz-transition': 'opacity .25s ease-out',
                        transition: 'opacity .25s ease-out'
                    });
                },
                buttonOnMouseOutFunction: function(button, image) {
                    button.css({
                        opacity: 0.75,
                        '-webkit-transition': 'opacity .25s ease-out',
                        '-moz-transition': 'opacity .25s ease-out',
                        transition: 'opacity .25s ease-out'}
                    );
                    image.css({
                        opacity:1,
                        '-webkit-transition': 'opacity .25s ease-out',
                        '-moz-transition': 'opacity .25s ease-out',
                        transition: 'opacity .25s ease-out'
                    });
                },
                popupX: 100,
                popupY: 100,
                popupHeight: 580,
                popupWidth: 730,
                popupName: 'pinterest'
            },
            settings = $.extend(defaults, options);
        
        var getButtonPosition = function(image) {
            var imagePosition = image.position(),
                imageWidth = image.width(),
                imageHeight = image.height();
            
            switch (settings.buttonAlignment) {
                case 'top-right':
                    return {
                        top: imagePosition.top + settings.buttonPadding,
                        left: imagePosition.left + imageWidth - settings.buttonWidth - settings.buttonPadding
                    };
                case 'bottom-right':
                    return {
                        top: imagePosition.top + imageHeight - settings.buttonHeight - settings.buttonPadding,
                        left: imagePosition.left + imageWidth - settings.buttonWidth - settings.buttonPadding
                    };
                case 'bottom-left':
                    return {
                        top: imagePosition.top + imageHeight - settings.buttonHeight - settings.buttonPadding,
                        left: imagePosition.left + settings.buttonPadding
                    };
                default: // 'top-left
                    return {
                        top: imagePosition.top + settings.buttonPadding,
                        left: imagePosition.left + settings.buttonPadding
                    };
            }
        };
        
        var getImageAbsoluteUrl = function(image) {
            var value = image.attr(settings.imageMediaAttribute);
            // These controls should be safe enough.
            // If not, the image is likely broken anyway.
            if (!value.match("^http://")) {
                var separator = (value.substr(0,1) === "/") ? "" : "/";
                value = window.location.origin + separator + value;
            }
            return value;
        };
        
        return this.each(function(index) {
            var currentImage = $(this),
                baseUrl = 'http://pinterest.com/pin/create/button/',
                button = $('<img src="' + settings.buttonUrl + '" id="pinme_button" />'),
                wrapperSelector = 'pinme_wrapper_' + index,
                wrapper = $('<span class="' + wrapperSelector + '" />');
            
            // Wraps all images so that we can use mouseenter and mouseleave
            // events, otherwise hovering on the button would trigger a mouseout.
            currentImage.wrap(wrapper);
            
            $('.' + wrapperSelector).on('mouseenter mouseleave', function(event) {
                var currentWrapper = $(this);
                if (event.type === 'mouseenter') {
                    // Gets the wrapped image again to obtain the needed parameters.
                    var imageMedia = getImageAbsoluteUrl(currentImage),
                        imageDescription = currentImage.attr(settings.imageDescriptionAttribute);
                    
                    var params = {
                        url: document.URL,
                        media: imageMedia,
                        description: imageDescription
                    };
                    
                    var buttonPosition = getButtonPosition(currentImage);
                    
                    button.css({
                        position: 'absolute',
                        top: buttonPosition.top,
                        left: buttonPosition.left,
                        cursor: 'pointer'
                    });
                    
                    settings.buttonWillShowFunction(button);
                    
                    button.on('click mouseover mouseout', function(event) {
                        var currentButton = $(this);
                        if (event.type === 'click') {
                            var url = baseUrl + '?' + $.param(params),
                                specs = 'screenX=' + settings.popupX + ',screenY=' + settings.popupY + ',width=' + settings.popupWidth + ',height=' + settings.popupHeight;
                            window.open(url, settings.popupName, specs);
                            event.preventDefault();
                            event.stopPropagation();
                        } else if (event.type === 'mouseover') {
                            settings.buttonOnMouseOverFunction(currentButton, currentImage);
                        } else if (event.type === 'mouseout'){
                            settings.buttonOnMouseOutFunction(currentButton, currentImage);
                        };
                    });
                    
                    button.appendTo(currentWrapper);
                } else {
                    button.off();
                    button.remove();
                }
                return false;
            });
        });
    };
})( jQuery );