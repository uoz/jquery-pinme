jQuery PinMe Plugin
=

**PinMe** is a jQuery plugin to easily share single images on Pinterest. 
Once installed, it adds a *Pin-it* button to selected images on a page to share them. 
The plugin is highly customizable: see the **options** for a list of things that can be modified.

Getting started
-

PinMe needs a selector to pick up the images:

    :::html
    <img src="..." class="pinme" />

Then add jQuery and PinMe scripts:

    :::html
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script src="/path/to/jquery.pinme.min.js"></script>

To fire PinMe you have two options. As the button is going to be dynamically positioned over each image, they need to be fully loaded for PinMe to be able to retrieve their dimensions.  
Therefore you can bind it to the `$(window).load()` event:

    :::javascript
    $(window).load(function() {
        $('.pinme').pinMe();
    });
or, to be on the safer side, you can use the **[imagesLoaded][1]** plugin.
    
    :::html
    ...
    <script src="/path/to/jquery.imagesloaded.min.js"></script>
    ...
    $(document).ready(function() {
        $('.pinme').imagesLoaded(function() {
            this.pinMe();
        });
    });

Options
-
    :::javascript
    imageMediaAttribute: 'src'
By default, the value of the `src` attribute is used to retrieve the absolute url of the image.  
Alternatively, you can use another attribute, like a `data-*` if, for example, you want to share a higher resolution version of the image.  
**Note that PinMe will transform any relative url to an absolute one** as required by Pinterest.

    :::javascript
    imageDescriptionAttribute: 'data-pin-description'
The HTML attribute of the image whose value is the description to be sent to Pinterest.

    :::javascript
    buttonUrl: 'img/pinit.png'
The url of the *Pin-it* button, in case you don't like the default one.
If you edit this, be sure to check the two options below.

    :::javascript
    buttonWidth: 49
The width of the custom button in pixels.

    :::javascript
    buttonHeight: 25
The height of the custom button in pixels.

    :::javascript
    buttonAlignment: 'top-left'
The position of the button relatively to the image. Options are `top-left`, `top-right`, `bottom-left` and `bottom-right`.

    :::javascript
    buttonPadding: 5
The distance of the button in pixels from the margin of the image.

    :::javascript
    buttonWillShowFunction: function(button) {
        button.css({
            opacity: 0.75
        });
    }
A function that gets called just before the button is added to the DOM, usefull for any extra initialization.

    :::javascript
    buttonOnMouseOverFunction: function(button, image) {
        button.css({
            opacity: 1.0,
            '-webkit-transition': 'opacity .25s ease-out',
            '-moz-transition': 'opacity .25s ease-out',
            transition: 'opacity .25s ease-out'
        });
        image.css({
            opacity:0.75,
            '-webkit-transition': 'opacity .25s ease-out',
            '-moz-transition': 'opacity .25s ease-out',
            transition: 'opacity .25s ease-out'
        });
    }
A function that gets triggered by the mouse over event of the button.

    :::javascript
    buttonOnMouseOutFunction: function(button, image) {
        button.css({
            opacity: 0.75,
            '-webkit-transition': 'opacity .25s ease-out',
            '-moz-transition': 'opacity .25s ease-out',
            transition: 'opacity .25s ease-out'}
        );
        image.css({
            opacity:1,
            '-webkit-transition': 'opacity .25s ease-out',
            '-moz-transition': 'opacity .25s ease-out',
            transition: 'opacity .25s ease-out'
        });
    }
A function that gets triggered by the mouse out event of the button.

    :::javascript
    popupX: 100
The distance of the popup from the left margin of the browser window in pixels.


    :::javascript
    popupY: 100
The distance of the popup from the top margin of the browser window in pixels.

    :::javascript
    popupHeight: 580
The height in pixels of the popup the will be open by the button.

    :::javascript
    popupWidth: 730
The width in pixels of the popup the will be open by the button.

    :::javascript
    popupName: 'pinterest'
The name of the popup the will be open by the button.

Example
-
    :::html
    <img src="path/to/my/image1.jpg" data-description="My beautiful image number 1" />
    <img src="path/to/my/image2.jpg" data-description="My beautiful image number 2" />

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script src="/path/to/jquery.pinme.min.js"></script>
    <script src="/path/to/jquery.imagesloaded.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        $('.pinme').imagesLoaded(function() {
            this.pinMe({
                buttonPadding: 8,
                buttonAlignment: 'top-right',
                imageDescriptionAttribute: 'data-description'
            });
        });
    });
    </script>

Version
-

1.0


License
-

WTFPL

 [1]: https://github.com/desandro/imagesloaded
